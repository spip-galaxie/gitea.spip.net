diff --git a/spip/ecrire/inc/rubriques.php b/spip/ecrire/inc/rubriques.php
index 2eded09..3d9103f 100644
--- a/spip/ecrire/inc/rubriques.php
+++ b/spip/ecrire/inc/rubriques.php
@@ -292,7 +292,7 @@ function propager_les_secteurs()
 	// reparer les rubriques qui n'ont pas l'id_secteur de leur parent
 	do {
 		$continuer = false;
-		$r = sql_select("A.id_rubrique AS id, R.id_secteur AS secteur, R.profondeur+1 as profondeur", "spip_rubriques AS A, spip_rubriques AS R", "A.id_parent = R.id_rubrique AND (A.id_secteur <> R.id_secteur OR A.profondeur <> R.profondeur+1)");
+		$r = sql_select("A.id_rubrique AS id, R.id_secteur AS secteur, R.profondeur+1 as profondeur", "spip_rubriques AS A, spip_rubriques AS R", "A.id_parent = R.id_rubrique AND A.id_rubrique <> R.id_rubrique AND (A.id_secteur <> R.id_secteur OR A.profondeur <> R.profondeur+1)");
 		while ($row = sql_fetch($r)) {
 			sql_update("spip_rubriques", array("id_secteur" => $row['secteur'],'profondeur' => $row['profondeur']), "id_rubrique=".$row['id']);
 			$continuer = true;
