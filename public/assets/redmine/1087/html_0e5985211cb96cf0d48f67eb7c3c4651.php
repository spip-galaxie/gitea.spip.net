<?php

/*
 * Squelette : plugins/paeco/squelettes/test.html
 * Date :      Tue, 21 May 2019 19:23:58 GMT
 * Compile :   Tue, 21 May 2019 19:24:14 GMT
 * Boucles :   _vieilles
 */ 

function BOUCLE_vieilleshtml_0e5985211cb96cf0d48f67eb7c3c4651(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_vieilles';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.titre AS titre_rang",
		"articles.id_article",
		"articles.lang",
		"articles.titre");
		$command['orderby'] = array();
		$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''));
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/paeco/squelettes/test.html','html_0e5985211cb96cf0d48f67eb7c3c4651','_vieilles',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
calculer_rang_smart($Pile[$SP]['titre_rang'], 'article', $Pile[$SP]['id_article'], $Pile[0]) .
'
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_vieilles @ plugins/paeco/squelettes/test.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/paeco/squelettes/test.html
// Temps de compilation total: 3.748 ms
//

function html_0e5985211cb96cf0d48f67eb7c3c4651($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = BOUCLE_vieilleshtml_0e5985211cb96cf0d48f67eb7c3c4651($Cache, $Pile, $doublons, $Numrows, $SP);

	return analyse_resultat_skel('html_0e5985211cb96cf0d48f67eb7c3c4651', $Cache, $page, 'plugins/paeco/squelettes/test.html');
}
?>
