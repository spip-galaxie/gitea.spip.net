Index: /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination.html
===================================================================
--- /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination.html	(revision 11287)
+++ /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination.html	(working copy)
@@ -2,7 +2,7 @@
 #SET{bornes,#ENV{page_courante}|bornes_pagination{#ENV{nombre_pages},10}}
 #SET{premiere, #GET{bornes}|reset}
 #SET{derniere, #GET{bornes}|end}
-#SET{separateur,'|'}
+#SET{separateur,#ENV{separateur,'|'}}
 
 [<a href='[(#ENV{url}|parametre_url{#ENV{debut},''})]##ENV{ancre}' class='lien_pagination' rel='nofollow'>(#GET{premiere}|>{1}|?{'...',''})</a> #GET*{separateur}]
 
Index: /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_page.html
===================================================================
--- /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_page.html	(revision 11287)
+++ /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_page.html	(working copy)
@@ -2,7 +2,7 @@
 #SET{bornes,#ENV{page_courante}|bornes_pagination{#ENV{nombre_pages},10}}
 #SET{premiere, #GET{bornes}|reset}
 #SET{derniere, #GET{bornes}|end}
-#SET{separateur,'|'}
+#SET{separateur,#ENV{separateur,'|'}}
 
 [<a href='[(#ENV{url}|parametre_url{#ENV{debut},''})]##ENV{ancre}' class='lien_pagination' rel='nofollow'>(#GET{premiere}|>{1}|?{'...',''})</a> #GET*{separateur}]
 
Index: /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_page_precedent_suivant.html
===================================================================
--- /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_page_precedent_suivant.html	(revision 11287)
+++ /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_page_precedent_suivant.html	(working copy)
@@ -2,7 +2,7 @@
 #SET{bornes,#ENV{page_courante}|bornes_pagination{#ENV{nombre_pages},10}}
 #SET{premiere, #GET{bornes}|reset}
 #SET{derniere, #GET{bornes}|end}
-#SET{separateur,'|'}
+#SET{separateur,#ENV{separateur,'|'}}
 
 [<a href='[(#ENV{url}|parametre_url{#ENV{debut},''})]##ENV{ancre}' class='lien_pagination' rel='nofollow'>(#GET{premiere}|>{1}|?{'...',''})</a> #GET*{separateur}]
 #SET{i,#ENV{page_courante}|moins{1}}
Index: /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_precedent_suivant.html
===================================================================
--- /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_precedent_suivant.html	(revision 11287)
+++ /Volumes/Bordel/Sites/kent1.sklunk.net/dist/modeles/pagination_precedent_suivant.html	(working copy)
@@ -2,7 +2,7 @@
 #SET{bornes,#ENV{page_courante}|bornes_pagination{#ENV{nombre_pages},10}}
 #SET{premiere, #GET{bornes}|reset}
 #SET{derniere, #GET{bornes}|end}
-#SET{separateur,'|'}
+#SET{separateur,#ENV{separateur,'|'}}
 #SET{i,#ENV{page_courante}|moins{1}}
 [(#GET{i}|>{0}|?{' '})[
 	(#SET{item, #GET{i}|moins{1}|mult{#ENV{pas}} })
