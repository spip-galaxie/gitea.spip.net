<?php
function testarg ($n, $p) {
	$champ = interprete_argument_balise($n, $p);
	$champ = str_replace(' ', '~', $champ);
	echo "(compil) arg$n=($champ) len=".strlen ($champ)."<br>";
	return $champ;

};

function balise_TESTARG_dist($p) {
	$p->interdire_scripts = false;
	$champ1 = testarg(1, $p);
	$champ2 = testarg(2, $p);
	$champ3 = testarg(3, $p);
	$p->code = "'-'";
	return $p;
}
