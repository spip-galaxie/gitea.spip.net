<?php
// http://doc.spip.org/@filtre_text_csv_dist
function filtre_text_csv_dist($t)
{
	$virg = substr_count($t, ',');
	$pvirg = substr_count($t, ';');
	$tab = substr_count($t, "\t");
	if ($virg > $pvirg)
		{ $sep = ','; $hs = '&#44;';}
	else	{ $sep = ';'; $hs = '&#59;'; $virg = $pvirg;}
	if ($tab > $virg) {$sep = "\t"; $hs = "\t";}

	$t = str_replace('""','&#34;',
			 preg_replace('/\r?\n/', "\n",
				      preg_replace('/\r/', "\n", $t)));
	preg_match_all('/"[^"]*"/', $t, $r);
	foreach($r[0] as $cell) 
		$t = str_replace($cell, 
			str_replace($sep, $hs,
				str_replace("\n", "<br />", 
					    substr($cell,1,-1))),
			$t);
	list($entete, $corps) = split("\n",$t,2);
	$caption = '';
	
	// sauter la ligne de tete formee seulement de separateurs 
	if (substr_count($entete, $sep) == strlen($entete)) {
		list($entete, $corps) = split("\n",$corps,2);
	}
	// si une seule colonne, en faire le titre
	if (preg_match("/^([^$sep]+)$sep+\$/", $entete, $l)) {
			$caption = "\n||" .  $l[1] . "|";
			list($entete, $corps) = split("\n",$corps,2);
	}
	//Cherche les repetitions continues des separateurs
	if (preg_match("/$sep$sep+/", $corps, $l)) {
	//Cherche le nombre de colonnes dans la seule source fiable, l'entete
	$tabcorps = explode ($sep,$entete);
	$rowspans = count ($tabcorps);
	//Puis on enleve le premier (ce sera $sep) et le dernier dernier (ce sera $endoftherow)
	$rowstotal = $rowspans-2;
	//On fait notre fussionneur
	$rowfussion= "<|";
	//On fait notre fussionneur final
	$endoftherow= "<";
	//On le multiplie par le nombre de colonnes
	$rowsinclus = str_repeat($rowfussion , $rowstotal);
	//On change les separaturs normaux par les fussionneurs
	$corps = eregi_replace("$sep$sep+","$sep$rowsinclus$endoftherow",$corps);
	}
	// si premiere colonne vide, le raccourci doit quand meme produire <th...
	if ($entete[0] == $sep) $entete = ' ' . $entete;
	if ($corps[strlen($corps)-1] <> "\n") $corps .= "\n";
	return propre($caption .
		"\n|{{" .
		str_replace($sep,'}}|{{',$entete) .
		"}}|" .
		"\n|" .
		str_replace($sep,'|',str_replace("\n", "|\n|",$corps)));
}
?>