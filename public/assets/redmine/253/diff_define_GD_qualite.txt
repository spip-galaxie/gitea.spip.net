Index: /Volumes/Bordel/Sites/kent1.sklunk.net/ecrire/inc/filtres_images.php
===================================================================
--- /Volumes/Bordel/Sites/kent1.sklunk.net/ecrire/inc/filtres_images.php	(revision 11287)
+++ /Volumes/Bordel/Sites/kent1.sklunk.net/ecrire/inc/filtres_images.php	(working copy)
@@ -196,7 +196,7 @@
 	return $ret;
 }
 // http://doc.spip.org/@image_imagejpg
-function image_imagejpg($img,$fichier,$qualite=85) {
+function image_imagejpg($img,$fichier,$qualite=_IMG_GD_QUALITE) {
 	$tmp = $fichier.".tmp";
 	$ret = imagejpeg($img,$tmp, $qualite);
 
@@ -210,7 +210,7 @@
 
 // $qualite est utilise pour la qualite de compression des jpeg
 // http://doc.spip.org/@image_gd_output
-function image_gd_output($img,$valeurs, $qualite=85){
+function image_gd_output($img,$valeurs, $qualite=_IMG_GD_QUALITE){
 	$fonction = "image_image".$valeurs['format_dest'];
 	$ret = false;
 	#un flag pour reperer les images gravees
@@ -2024,7 +2024,7 @@
 function image_aplatir($im, $format='jpg', $coul='000000', $qualite=NULL)
 {
 	if ($qualite===NULL){
-		if ($format=='jpg') $qualite=85;
+		if ($format=='jpg') $qualite=_IMG_GD_QUALITE;
 		elseif ($format=='png') $qualite=0;
 		else $qualite=128;
 	}
Index: /Volumes/Bordel/Sites/kent1.sklunk.net/ecrire/inc/utils.php
===================================================================
--- /Volumes/Bordel/Sites/kent1.sklunk.net/ecrire/inc/utils.php	(revision 11287)
+++ /Volumes/Bordel/Sites/kent1.sklunk.net/ecrire/inc/utils.php	(working copy)
@@ -1190,6 +1190,7 @@
 
 	# nombre de pixels maxi pour calcul de la vignette avec gd
 	define('_IMG_GD_MAX_PIXELS', isset($GLOBALS['meta']['max_taille_vignettes'])?$GLOBALS['meta']['max_taille_vignettes']:0);
+	define('_IMG_GD_QUALITE', 85);
 }
 
 // Annuler les magic quotes \' sur GET POST COOKIE et GLOBALS ;
