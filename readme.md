# Thème du site git.spip.net

## Introduction

> Le dépot permet de personnaliser la mise en forme du site git.spip.net

## Documentation

La structure des squelettes et autres éléments sont détaillées sur la documentation officielle de gitea :
* https://docs.gitea.io/en-us/customizing-gitea/

Les gabarits surchargeables sont définis dans le dépot source :
* https://github.com/go-gitea/gitea/tree/master/templates


## Contribuer

Les correctifs sont avec plaisir appréciés et bienvenus.